 .ORG   0x0000                  // Tells the next instruction to be written
 RJMP   main                    // State that the program begins at the main label

 main:

.DSEG 
A: .DB 	 28,122,80,	42,	54,	122,98,	42,	99,	58,	124,29,	21,113,	85,	30,	35,	41,	98,	103,68,	15,	50,	31,	80,	54 ,47,	37,	23,	96,	59,	47,	84,	26,	84,	72,	51,	118,119 ,38,121,45,	21,	87,	91,	20,	69,	98,	119,15,	89,	47 ,40,	71,	105,76,	31,	65,	109,30,	127,110,17,	64,	64 ,45,	20,	113,86,	86,	51,	104,115,61,	103,60,	113,44 ,101,107,33,	63,	39,	47,	120,20,	41,	64,	102,59,	86 ,9,	42,	118,26,	83,	123,10,	82,	47,	108,127,4,	66,75,	26,	117,80,	47,	111,38,	22,	98,	101,	92,		100,	48 ,90,	9,		71,		36,		90,		95,		4,		94,		72,		29,		77,		118,	78, 81,	75,		97,		127,	22,		8,		96,		80,	100,88,	69,	114,16 ,25,	109,74,	3,	126,56,	99,	15,	69,	73,	76,19,97,59,84,	102,53,	30,	34,	33,	105,75,	102,60,	121,93 
B: .DB 	 102, 106, 65, 114, 25,	45,	39,	58,	119, 121, 29, 70, 123, 61,	89,	115, 37, 111, 41, 68, 62, 75, 42, 96, 108, 72, 111,	35,	94,	42,	4, 20, 87,	122, 20, 78, 0,	69,	56, 79,	37,	68,	3, 116,	33,	59,	106, 42, 45, 64, 12, 5, 99,	112, 95, 88, 54, 89, 33, 93, 1,	8, 47,	0,	30, 3, 51, 114,	35,	90,	75,	82,	39,	120, 17, 50, 80, 45, 25, 13,34,	124, 11, 89, 94, 86, 83,	52,	22,	115, 2, 50,	70,	34,	50,	32,	2,	14,	80,	24,	38,	17,	107, 11, 33, 3,	64,	74,	121, 28, 115, 75, 62, 44, 88,	71,	124, 48, 110, 1, 95, 20, 19, 0,	23,	78,	13,	63,	54,	84, 5, 31,	11,	25,	26,	96,	0,	57,	20,	104, 108, 5, 63, 94, 7,	66,	34,	62,	46,	92,	89,	25,	57,	78,	57,	47, 28,	99,	126, 24, 60, 119, 85,	7,	126, 62, 101, 3, 104
;I transposed B and the colomns of it are in now.
C: .BYTE 507  ;;reservers 13*13*3 bytes. so 13*13 24bits.which should be enough.
;macros

;macros can make some things maybe easier....
.MACRO ADDMULRESULTS ; Start macro definition 
adc r4, r0;
adc r5, r1;
adc r6, r20;    taking care of overflow 16bits value
.ENDMACRO ; End macro definition

.MACRO LOADMUL ; Start macro definition 
ld r0, Y+   ; 
ld r1, Z+   ; Load second into register 1
cp r18, r17
brne skipmov
add r4, r0; add the rows   
skipmov:
cp r18, r15
brne skipmov2
adc r4, r1; add the column  
skipmov2:
mul r0, r1  ; multilicate
.ENDMACRO ; End macro definition

.CSEG
 cli ;
 LDI    r16, 0xFF               // Load the immedate value 0xFF (all bits 1) into register 16
 OUT    DDRB, r16               // Set Data Direction Register B to output for all pins

;call init_Ex1               
;call USART_Init
;transmit something

;RJMP  main 

; load the pointers
ldi r30,low(A) ; 
ldi r31,high(A) ;
ldi r28,low(B) ; 
ldi r29,high(B) ; 
ldi r26,low(C) ;  
ldi r27,high(C) ; 

ldi r16,0   ; overall register2
ldi r19,0   ; overall register
ldi r18,0   ; nextrow register
ldi r17,0   ; mulcount register
mov r15, r17   ; whichrow register
ldi r21, 0   ; count2 register
mov r25, r29;
mov r24, r28; store B column start points

loopmain:
LOADMUL
ADDMULRESULTS
inc r17
cpi r17, 13
breq store

RJMP  loopmain ; go back
; store the result into one element of C
store:
st X+, r4    ;   store the calculated values..
st X+, r5    ; 
st X+, r6    ; 
ldi r17,0    ; reset
mov r4,r20   ; reset
mov r5,r20   ; reset
mov r6,r20   ; reset



inc r19
cpi r19, 169
breq inccounter
inc r18
cpi r18, 13
breq changerow  ; goto next row. set column back to begin

;continue withc same row. next column
subi r30, 13 ; Subtract low byte with 13
sbci r31, 0 ;  Subtract with carry 
RJMP   loopmain

changerow:
;row will continue now
mov r29, r25;
mov r28, r24; restore B column start points
ldi r18, 0;
inc r15;

RJMP   loopmain 

inccounter:
;reset
ldi r30,low(A) ; 
ldi r31,high(A) ;
ldi r28,low(B) ; 
ldi r29,high(B) ; 
ldi r26,low(C) ;  
ldi r27,high(C) ; 

ldi r19, 0;
inc r16;
cpi r16, 250 //250 iterations
breq nextcounter
RJMP   loopmain

nextcounter:
ldi r16, 0;
inc r21;
cpi r21, 40 //10000 iterations
breq exit
RJMP   loopmain


exit:
sbis portb,5
sbi portb,5
sei
;call USART_Transmit
RJMP   exit


USART_Init:
  ldi r16, 0b1100111        ;103   16MHZ
   ldi r17, 0b0
   STS    UBRR0H, r17
   STS    UBRR0L, r16
   ; Enable transmitter
   ldi    r16, (0<<RXEN0)|(1<<TXEN0)
   STS    UCSR0B,r16
   ; Set frame format: 8data, 2stop bit
   ldi    r16, (1<<USBS0)|(3<<UCSZ00); (1<<UCSZ00)|(1<<UCSZ01) ;
   STS    UCSR0C,r16
   ret


   USART_Transmit:
   ; Wait for empty transmit buffer
   push r17
   LDS     r17, UCSR0A
   sbrs    r17, UDRE0
   rjmp    USART_Transmit
   ; Put data (r16) into buffer, sends the data
   ldi r17, 0x31 ; 
   STS    UDR0,r17
   pop r17
   ret